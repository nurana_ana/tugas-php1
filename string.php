<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>String PHP</title>
</head>
<body>
    <h1>Berlatih String PHP</h1>
    <h3> Soal No 1</h3>
    <?php   
        /* 
            SOAL NO 1
            Tunjukan dengan menggunakan echo berapa panjang dari string yang diberikan berikut! Tunjukkan juga jumlah kata di dalam kalimat tersebut! 

            Contoh: 
            $string = "PHP is never old";
            Output:
            Panjang string: 16, 
            Jumlah kata: 4 
        */

        $sentence1 = "Hello PHP!"; // Panjang string 10, jumlah kata: 2
        echo "First sentence : ". $sentence1 . "<br>";
        echo "Panjang String : ". strlen("$sentence1") . "<br>";
        echo "Jumlah Kata : ". str_word_count("$sentence1") . "<br><br>";

        $sentence2 = "I'm ready for the challenges"; // Panjang string: 28,  jumlah kata: 5
        echo "Second sentence : ". $sentence2 . "<br>";
        echo "Panjang String : ". strlen("$sentence2") . "<br>";
        echo "Jumlah Kata : ". str_word_count("$sentence2") . "<br><br>";
        

          /* 
            SOAL NO 2
            Mengambil kata pada string dan karakter-karakter yang ada di dalamnya. 
            
            
        */
        echo "<h3> Soal No 2</h3>";
      
        $string2 = "I love PHP";
         echo "<label>String: </label> \"$string2\" <br>";
        echo "Kata pertama: " . substr($string2, 0, 1) . "<br>" ; 
        // Lanjutkan di bawah ini
        echo "Kata kedua: " . substr($string2, 2, 5) . "<br>" ; 
        echo "Kata Ketiga: " . substr($string2, 6, 8) . "<br>" ;
        
        /*
            SOAL NO 3
            Mengubah karakter atau kata yang ada di dalam sebuah string.
        */
        echo "<h3> Soal No 3 </h3>";
        
        $string3 = "PHP is old but sexy!";
        echo "Kalimat ketiga : ". $string3. "<br>";
        echo "Ganti Kalimat ketiga : ". str_replace("sexy", "awesome",$string3); 
        // OUTPUT : "PHP is old but awesome"
       // untuk strlen dan str replace cari di w3school-php-strings

        
    

    ?>
</body>
</html>